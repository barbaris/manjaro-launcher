echo Hello world!

cd ~/
mkdir .ssh
cd .ssh/
wget https://gitlab.com/barbaris/manjaro-launcher/-/raw/main/id_rsa.gpg -O ~/.ssh/id_rsa.gpg
read passphrase
gpg --passphrase $passphrase --output ~/.ssh/id_rsa --decrypt ~/.ssh/id_rsa.gpg
chmod 600 ~/.ssh/id_rsa

cd ~/
mkdir Projects
cd Projects
ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
git clone git@gitlab.com:barbaris/manjaro-kickstarter.git
cd manjaro-kickstarter/
sudo sh start.sh
